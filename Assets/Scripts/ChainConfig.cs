using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ChainConfig
{
#if DEMO_CHAIN
    public static string Protocol = "https";

    public static string Host = "demo-chain.liquiduniverse.io";

    public static int Port = 443;

    public static string ChainId = "df383d1cc33cbb9665538c604daac13706978566e17e5fd5f897eff68b88e1e4";
#elif DEV_CHAIN
    public static string Protocol = "https";

    public static string Host = "dev-chain.liquiduniverse.io";

    public static int Port = 443;

    public static string ChainId = "df383d1cc33cbb9665538c604daac13706978566e17e5fd5f897eff68b88e1e4";
#else

    /*
     * Standard-Configuration. Was also used for first Demo
     */
    /*
    public static string Protocol = "https";

    public static string Host = "dev-chain.liquiduniverse.io";

    public static int Port = 443;

    public static string ChainId = "df383d1cc33cbb9665538c604daac13706978566e17e5fd5f897eff68b88e1e4";
    
    */

    public static string Protocol = "https";

    public static string Host = "dev-chain.liquiduniverse.io";  // just change this to your workspace-url (without Protocol)

    public static int Port = 443;

    public static string ChainId = "df383d1cc33cbb9665538c604daac13706978566e17e5fd5f897eff68b88e1e4";

#endif

    /*
     * keys and accounts on Gitpod:
     *
     * key: 5J8cuSLqqsPi25s3L6CRqvPNzaS4iaCZMYmpQVBrktkV5TneUyK
     *
     * aaaaaaaaa, aaaaaaaab, aaaaaaaac, aaaaaaaad ... 
     *
     *
     */
}
