using System;
using System.Text;
using System.Threading.Tasks;
using DfuseGraphQl;
using GraphQlClient.Core;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class SimpleDfuseGraphQlClient: MonoBehaviour
{
    public GraphApi dfuseGraphApi;
    private GraphApi.Query query;

    async void Start()
    {
        // query = dfuseGraphApi.GetQueryByName("searchTransactionForward ($cursor: String)", GraphApi.Query.Type.Query);
        // query.SetArgs("\r\n    query: \"(receiver:eosio.token action:transfer)\",\r\n    cursor: $cursor");
        
        query = dfuseGraphApi.GetQueryByName("subscription2 ($cursor: String) ", GraphApi.Query.Type.Subscription);
        query.SetArgs("\r\n    query: \"(account:planets OR account:admin)\",\r\n    cursor: $cursor");
        
        print(query.query);
        
        string graphQlApiUrl = $"{ChainConfig.Protocol}://{ChainConfig.Host}/graphql";
        print("using " + graphQlApiUrl);

        var searchTransactionForwardResponse = await SearchTransactionsForwardPostAsync(graphQlApiUrl, query.query);
        ConsumeActions(searchTransactionForwardResponse.Trace.MatchingActions);
        
        var searchTransactionBackwardResponse = await SearchTransactionsBackwardPostAsync(graphQlApiUrl, query.query);
        ConsumeActions(searchTransactionBackwardResponse.Trace.MatchingActions);
    }

    private static async Task<SearchTransactionsForward> SearchTransactionsForwardPostAsync(string url, string details){
        string jsonData = JsonConvert.SerializeObject(new{query = details});
        byte[] postData = Encoding.ASCII.GetBytes(jsonData);
        UnityWebRequest request = UnityWebRequest.Post(url, UnityWebRequest.kHttpVerbPOST);
        request.uploadHandler = new UploadHandlerRaw(postData);
        request.SetRequestHeader("Content-Type", "application/json");
            
        try{
            await request.SendWebRequest();
        }
        catch(Exception e){
            Debug.Log("Testing exceptions");
        }
        Debug.Log(request.downloadHandler.text);
            
        return JsonConvert.DeserializeObject<SearchTransactionsForward>(request.downloadHandler.text);;
    }

    private static async Task<SearchTransactionsBackward> SearchTransactionsBackwardPostAsync(string url, string details){
        string jsonData = JsonConvert.SerializeObject(new{query = details});
        byte[] postData = Encoding.ASCII.GetBytes(jsonData);
        UnityWebRequest request = UnityWebRequest.Post(url, UnityWebRequest.kHttpVerbPOST);
        request.uploadHandler = new UploadHandlerRaw(postData);
        request.SetRequestHeader("Content-Type", "application/json");
            
        try{
            await request.SendWebRequest();
        }
        catch(Exception e){
            Debug.Log("Testing exceptions");
        }
        Debug.Log(request.downloadHandler.text);
            
        return JsonConvert.DeserializeObject<SearchTransactionsBackward>(request.downloadHandler.text);;
    }

    private void ConsumeActions(MatchingAction[] matchingActions)
    {

    }
}